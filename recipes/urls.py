from django.urls import path

from recipes.views import (
    log_rating,
    RecipeListView,
    RecipeUpdateView,
    RecipeDetailView,
    RecipeCreateView,
    RecipeDeleteView,
)

urlpatterns = [
    path("", RecipeListView.as_view(), name="recipes_list"),
    path(
        "detail/<slug:slug>/", RecipeDetailView.as_view(), name="recipe_detail"
    ),
    path("<slug:slug>/edit/", RecipeUpdateView.as_view(), name="recipe_edit"),
    path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),
    path("new/", RecipeCreateView.as_view(), name="recipe_new"),
    path("<slug:slug>", RecipeDeleteView.as_view(), name="recipe_delete"),
]
